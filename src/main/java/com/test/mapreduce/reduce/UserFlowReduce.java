package com.test.mapreduce.reduce;

import com.test.mapreduce.bean.UserFlow;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class UserFlowReduce extends Reducer<Text, UserFlow, Text, UserFlow> {

    @Override
    protected void reduce(Text key, Iterable<UserFlow> values, Context context)throws IOException, InterruptedException {

        long sum_upFlow = 0;
        long sum_downFlow = 0;

        // 1 遍历所用bean，将其中的上行流量，下行流量分别累加
        for (UserFlow userFlow : values) {
            sum_upFlow += userFlow.getUpFlow();
            sum_downFlow += userFlow.getDownFlow();
        }

        // 2 封装对象
        UserFlow resultBean = new UserFlow(sum_upFlow, sum_downFlow);

        // 3 写出
        context.write(key, resultBean);
    }


}
