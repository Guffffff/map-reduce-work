package com.test.mapreduce.map;

import com.test.mapreduce.bean.UserFlow;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class UserFlowMapper extends Mapper<LongWritable, Text, Text, UserFlow> {

    Text k = new Text();
    UserFlow v = new UserFlow();

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        // 1 获取一行
        String line = value.toString();

        // 2 切割字段
        String[] fields = line.split("\t");

        // 3 封装对象
        // 取出手机号码
        String phoneNum = fields[1];
        k.set(phoneNum);

        // 取出上行流量和下行流量
        v.setUpFlow(Long.parseLong(fields[fields.length - 3]));
        v.setDownFlow(Long.parseLong(fields[fields.length - 2]));

        // 4 写出
        context.write(k, v);
    }

}
